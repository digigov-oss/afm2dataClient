import soapClient from './soapClient.js';
import {generateAuditRecord,AuditRecord} from '@digigov-oss/gsis-audit-record-db';
import config from './config.json'; 


export type AuditInit = AuditRecord;
export type afm2dataOutputRecord = {
    afmInput: string; 
    amkaCurrent: string; 
    lastDataLogDate: string;
    adt: string; 
    nationalityCountry: string;
    nationalityCountryCode: string; 
    afm: string;
    birthSurnameEl: string; 
    idSurnameEl: string; 
    firstNameEl: string; 
    fatherNameEl: string; 
    motherNameEl: string;  
    birthSurnameEn: string; 
    idSurnameEn: string; 
    firstNameEn:string; 
    fatherNameEn:string; 
    motherNameEn: string; 
    birthDate:string; 
    fictitiousBirthDateIndication: string; 
    deathDate: string; 
    callSequenceId: string; 
    callSequenceDate:string; 
}

export type errorRecord = {
    errorCode:string;
    errorDescr:string;
}

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overides = {
   prod?:boolean;
   auditInit?: AuditRecord;
   auditStoragePath?: string;
}

/**
 * 
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overides Overides;
 * @returns afm2dataOutputRecord | errorRecord
 */
export const getAmka = async (afm:string, user:string, pass:string, overides?:Overides) => {
    const prod = overides?.prod ?? false;
    const auditInit = overides?.auditInit ?? {} as AuditRecord;
    const auditStoragePath = overides?.auditStoragePath ?? "/tmp"
    const wsdl = prod==true? config.prod.wsdl : config.test.wsdl;
    const auditRecord = generateAuditRecord(auditInit, auditStoragePath);
    if (!auditRecord) throw new Error('Audit record is not initialized');
  
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord);
        const Identity = await s.getAmka(afm);
        return Identity;
       } catch (error) {
           throw(error);
       }

}
export default getAmka;