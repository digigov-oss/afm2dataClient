import {parse, stringify, toJSON, fromJSON} from 'flatted';

import thesoap from 'soap';
let soap:any = thesoap;
try {
   soap = require('soap');
} catch (error) {
  //my hackish way to make soap work on both esm and cjs
  //theshoap on esm is undefined
  //On esm require is not defined, however on cjs require can be used.
  //So we try to use require and if it fails we use the thesoap module
}

import {AuditRecord} from '@digigov-oss/gsis-audit-record-db';
/**
 * SOAP client for getNncIdentity
 * @class Soap
 * @description SOAP client for getNncIdentity
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 */
class Soap {
  private _client: any;
  private _wsdl: string;
  private _username: string;
  private _password: string;
  private _auditRecord: AuditRecord;


  constructor(wsdl: string, username: string, password: string,auditRecord: AuditRecord) {
    this._wsdl = "afm2dataService.wsdl";
    this._username = username;
    this._password = password;
    this._auditRecord = auditRecord;
  }

  public async init() {
    try {
      const client = await soap.createClientAsync(this._wsdl, {
        wsdl_headers: {
          'Authorization': 'Basic ' + Buffer.from(`${this._username}:${this._password}`).toString('base64'),
        },
      });

      this._client = client;
      return client;
    } catch (e) {
      throw e;
    }
  }

  public async getAmka(vat:string) {
    try {
      const client = await this.init();
      var options = {
        hasNonce: true,
        actor: 'actor'
      };
      var wsSecurity = new soap.WSSecurity(this._username, this._password, options)
      client.setSecurity(wsSecurity);
      const auditRecord = this._auditRecord;
      const args = {
        auditRecord: auditRecord,
        afm2dataInputRecord:{
            afm:vat
        }
      }

      //return stringify(client);
      const result = await client.afm2dataAsync(args);
      return result[0];//.getNncIdentityOutputRecord;
    } catch (e) {
      throw e;
    }
  }
}

export default Soap;